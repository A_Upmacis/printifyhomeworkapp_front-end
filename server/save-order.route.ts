import { Request, Response } from 'express';
import { replaceOrAddImportedOrder } from '../src/assets/data/existing_order_data';

export function saveOrder(req: Request, res: Response) {

  const id = req.params['id'];
  const orderToImport = req.body;
  const modifiedExistingOrders = replaceOrAddImportedOrder(id, orderToImport);

  res.status(200).json(modifiedExistingOrders);
}
