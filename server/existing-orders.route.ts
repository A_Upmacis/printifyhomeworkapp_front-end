import { Request, Response } from 'express';
import { getExistingOrders } from '../src/assets/data/existing_order_data';

export function existingOrders(req: Request, res: Response) {

  const pageSize: number = parseInt(req.query.size || 10, 10);
  const pageNumber: number = parseInt(req.query.page || 1, 10);
  const ordersAndPager = getExistingOrders(pageSize, pageNumber);
  const pageOfOrders = ordersAndPager[0];
  const pager = ordersAndPager[1];

  res.status(200).json({pageOfOrders, pager});
}
