import { Request, Response } from 'express';
import { getImportOrders } from '../src/assets/data/import_order_data';

export function importOrders(req: Request, res: Response) {

  const pageSize: number = parseInt(req.query.size || 10, 10);
  const pageNumber: number = parseInt(req.query.page || 1, 10);
  const searchValue: string = req.query.search || '';
  const ordersAndPager = getImportOrders(pageSize, pageNumber, searchValue);
  const pageOfOrders = ordersAndPager[0];
  const pager = ordersAndPager[1];

  res.status(200).json({pageOfOrders, pager});
}
