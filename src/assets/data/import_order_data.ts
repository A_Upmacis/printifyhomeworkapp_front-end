import { Order } from '../../app/model/order';
import { Pager } from '../../app/model/pager';
import { paginate } from '../../app/pagination/pagination.component';

// Define import orders data
export const IMPORT_ORDERS: any = [
  {
    id: 1,
    customer: 'Custom Customer',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -35.15,
    price: +20,
    fulfillment: 'Custom Status',
    order_volume: '154.39$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 1',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          },
          {
            name: 'Heavy Wool Socks 2'
          },
          {
            name: 'Crew Socks 2'
          },
          {
            name: 'Light Socks 2'
          },
          {
            name: 'Heavy Wool Socks 3'
          },
          {
            name: 'Crew Socks 3'
          },
          {
            name: 'Light Socks 3'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks 1',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks 2',
        SKU: 'SOCK_301_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks 3',
        SKU: 'SOCK_401_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks 4',
        SKU: 'SOCK_501_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 2,
    customer: 'John Doe',
    created: 'May, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.40$',
    SKU: 'SOCK_102_BIG_1',
    products: []
  },
  {
    id: 3,
    customer: 'John Doe Import',
    created: 'Dec, 15, 16:40 am',
    revenue: +5.15,
    cost: -75.15,
    price: +70,
    fulfillment: 'Custom Status',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 3',
        SKU: 'SOCK_101_BIG_1',
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
        ]
      },
      {
        name: 'Christmas Dog Socks Product 4',
        SKU: 'SOCK_301_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 4,
    customer: 'John Doe',
    created: 'Jul, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.41$',
    SKU: 'SOCK_103_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 4',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 5,
    customer: 'John Doe',
    created: 'Aug, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.42$',
    SKU: 'SOCK_104_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 5',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 6,
    customer: 'John Doe',
    created: 'Sep, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_105_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 6',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 7,
    customer: 'John Doe',
    created: 'Oct, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.44$',
    SKU: 'SOCK_106_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 7',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 8,
    customer: 'John Doe',
    created: 'Nov, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.45$',
    SKU: 'SOCK_107_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 8',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 9,
    customer: 'John Doe',
    created: 'Dec, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.46$',
    SKU: 'SOCK_108_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 9',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 10,
    customer: 'John Doe',
    created: 'Jan, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.47$',
    SKU: 'SOCK_110_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 10',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 11,
    customer: 'John Doe',
    created: 'Feb, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.49$',
    SKU: 'SOCK_111_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 11',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 12,
    customer: 'John Doe',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.50$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 12',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 13,
    customer: 'John Doe',
    created: 'May, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 13',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 14,
    customer: 'John Doe',
    created: 'Jun, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 14',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 15,
    customer: 'John Doe',
    created: 'Jul, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 15',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 16,
    customer: 'John Doe',
    created: 'Aug, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 16',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 17,
    customer: 'John Doe',
    created: 'Sep, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 17',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 18,
    customer: 'John Doe',
    created: 'Oct, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 18',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 19,
    customer: 'John Doe',
    created: 'Nov, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 19',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 20,
    customer: 'John Doe',
    created: 'Dec, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 20',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 21,
    customer: 'John Doe',
    created: 'Jan 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 21',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 22,
    customer: 'John Doe',
    created: 'Feb, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 22',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 23,
    customer: 'John Doe',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 23',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 24,
    customer: 'John Doe',
    created: 'Apr, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 24',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 25,
    customer: 'John Doe',
    created: 'May, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 25',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 26,
    customer: 'John Doe',
    created: 'Jun, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 26',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 27,
    customer: 'John Doe',
    created: 'Jul, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 27',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 28,
    customer: 'John Doe',
    created: 'Aug, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 28',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 29,
    customer: 'John Doe',
    created: 'Sep, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 29',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 30,
    customer: 'John Doe',
    created: 'Oct, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 30',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 31,
    customer: 'John Doe',
    created: 'Nov, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 31',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 32,
    customer: 'John Doe',
    created: 'Dec, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 32',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 33,
    customer: 'John Doe',
    created: 'Jan, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 33',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 34,
    customer: 'John Doe',
    created: 'Feb, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 34',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 35,
    customer: 'John Doe',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 35',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 36,
    customer: 'John Doe',
    created: 'Apr, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 36',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 37,
    customer: 'John Doe',
    created: 'May, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 37',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 38,
    customer: 'John Doe',
    created: 'Jun, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 38',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 39,
    customer: 'John Doe',
    created: 'Jul, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 39',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 40,
    customer: 'John Doe',
    created: 'Aug, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 40',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 41,
    customer: 'John Doe',
    created: 'Sep, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 41',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 42,
    customer: 'John Doe',
    created: 'Oct, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 42',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 43,
    customer: 'John Doe',
    created: 'Nov, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 43',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 44,
    customer: 'John Doe',
    created: 'Dec, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 44',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 45,
    customer: 'John Doe',
    created: 'Jan, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 45',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 46,
    customer: 'John Doe',
    created: 'Feb, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 46',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 47,
    customer: 'John Doe',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 47',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 48,
    customer: 'John Doe',
    created: 'Apr, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 48',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
];

// Get requested import orders page
export function getImportOrders(
  pageSize: number,
  pageNumber: number,
  searchValue: string
) {

  let orders: Order[] = Object.values(IMPORT_ORDERS);
  let pager: Pager;
  let pageOfOrders: Order[] = [];
  const ordersAndPager = [];

  // If search string is defined and import orders is array
  if (searchValue !== '' && Array.isArray(orders)) {

    // Return import orders that math search value string
    orders = orders.filter(currentValue => {

      // Define current order 1st array level keys
      const currentValueKeys = Object.keys(currentValue);

      // Define keys that should be added to search
      const keysForSearch = ['id', 'customer', 'order_volume', 'SKU'];

      return currentValueKeys
        .some(currentKeyValue => {

          // If this key matches searchable keys and search string by RegExp matches this key value, add this product to the orders array
          if (
            keysForSearch.some(keyForSearch => (keyForSearch === currentKeyValue)) &&
            new RegExp(searchValue, 'gi').test(currentValue[currentKeyValue])
          ) {
            return true;
          }
        });
    });
  }

  pager = paginate(orders.length, pageNumber, pageSize);
  pageOfOrders = orders.slice(pager.startIndex, pager.endIndex + 1);
  ordersAndPager.push(pageOfOrders, pager);

  // Return import orders required page according to search string if such has been defined
  return ordersAndPager;
}
