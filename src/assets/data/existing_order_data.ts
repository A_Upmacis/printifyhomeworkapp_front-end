import { Order } from '../../app/model/order';
import { Pager } from '../../app/model/pager';
import { paginate } from '../../app/pagination/pagination.component';

// Define existing orders data
export const EXISTING_ORDERS: any = [
  {
    id: 1,
    customer: 'Existing Order: John Doe',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.40$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 1',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks 1',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Light Socks'
          }
        ]
      },
    ]
  },
  {
    id: 3,
    customer: 'Existing Order: John Doe',
    created: 'Jun, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.41$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 3',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          }
        ]
      }
    ]
  },
  {
    id: 39,
    customer: 'Existing Order: John Doe',
    created: 'Jul, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.42$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 39',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 40,
    customer: 'Existing Order: John Doe',
    created: 'Aug, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 40',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 41,
    customer: 'Existing Order: John Doe',
    created: 'Sep, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.44$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 41',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 42,
    customer: 'Existing Order: John Doe',
    created: 'Oct, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.45$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 42',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 43,
    customer: 'Existing Order: John Doe',
    created: 'Nov, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 43',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 44,
    customer: 'Existing Order: John Doe',
    created: 'Dec, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 44',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 45,
    customer: 'Existing Order: John Doe',
    created: 'Jan, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 45',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 46,
    customer: 'Existing Order: John Doe',
    created: 'Feb, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 46',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 47,
    customer: 'Existing Order: John Doe',
    created: 'Mar, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 47',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  },
  {
    id: 48,
    customer: 'Existing Order: John Doe',
    created: 'Apr, 15, 16:40 am',
    revenue: +5.15,
    cost: -15.15,
    price: +10,
    fulfillment: 'In Production',
    order_volume: '54.43$',
    SKU: 'SOCK_101_BIG_1',
    products: [
      {
        name: 'Christmas Dog Socks Product 48',
        SKU: 'SOCK_101_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      },
      {
        name: 'Christmas Cat Socks',
        SKU: 'SOCK_201_BIG_1',
        variants: [
          {
            name: 'Heavy Wool Socks'
          },
          {
            name: 'Crew Socks'
          },
          {
            name: 'Light Socks'
          }
        ]
      }
    ]
  }
];

// Add imported order or replace if there is existing one with the same id
export function replaceOrAddImportedOrder(orderId: number, order: Order) {

  let thisIndex = 0;

  // If imported order id exists
  if (EXISTING_ORDERS.some((order: Order, index) => {

      thisIndex = index;
      return order.id == orderId;
    }
  )) {

    // Replace existing order with the same id
    EXISTING_ORDERS.splice(thisIndex, 1, order);

    // If imported order id does not exist
  } else {

    // Add this object to the beginning of existing order array
    EXISTING_ORDERS.unshift(order);
  }

  // return modified existing order array
  return EXISTING_ORDERS;
}

// Get requested existing orders page
export function getExistingOrders(pageSize: number, pageNumber: number) {

  const orders: Order[] = Object.values(EXISTING_ORDERS);
  let pager: Pager;
  let pageOfOrders: Order[] = [];
  const ordersAndPager = [];

  pager = paginate(orders.length, pageNumber, pageSize);
  pageOfOrders = orders.slice(pager.startIndex, pager.endIndex + 1);
  ordersAndPager.push(pageOfOrders, pager);

  // Return existing orders required page and pagination info
  return ordersAndPager;
}
