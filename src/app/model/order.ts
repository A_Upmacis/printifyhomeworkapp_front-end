
export interface Variant {
  name: string;
}

export interface Product {
  name: string;
  SKU: string;
  variants: {
    [key: string]: Variant
  };
}

export interface Order {
  id: number;
  customer: string;
  created: string;
  revenue: number;
  cost: number;
  price: number;
  fulfillment: string;
  orderVolume: string;
  SKU: string;
  products: {
    [key: string]: Product
  };
}
