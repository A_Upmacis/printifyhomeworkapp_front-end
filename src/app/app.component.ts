import { Component, OnInit } from '@angular/core';
import { ModalService } from './modal';
import { Observable } from 'rxjs';
import { Order } from './model/order';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  existingOrdersAndPagination$: Observable<any[]>;
  private currentPage = 1;

  constructor(private apiService: ApiService, private modalService: ModalService) {
  }

  // Get existing orders, when component is initialized
  ngOnInit() {
    this.existingOrdersAndPagination$ = this.apiService.loadOrders('existing');
  }

  // Get order list according to clicked page from pagination
  public goToPage(page: number) {

    this.currentPage = page;

    this.existingOrdersAndPagination$ = this.apiService.loadOrders(
      'existing',
      this.currentPage,
      ''
    );
  }

  // Save imported order by subscribing to http put call and getting changed existing order list
  saveOrder(importOrder: Order) {

    this.apiService.saveOrder(importOrder)
      .subscribe(
        () => {
          this.existingOrdersAndPagination$ = this.apiService.loadOrders('existing');
        }
      );
  }

  // Open modal
  openModal(id: string) {
    this.modalService.open(id);
  }
}
