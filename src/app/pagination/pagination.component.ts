import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent {

  public pagesArray: Array<number> = [];
  public currentPage = 1;

  @Input()
  set pagination(pagination: any) {

    if (pagination) {
      this.pagesArray = pagination.pages;
      this.currentPage = pagination.currentPage;
    }
  }

  // Define custom event emitter for switching between pages
  @Output()
  goToPage = new EventEmitter<number>();

  // Emit custom event for switching between pages
  public setPage(pageNumber: number) {

    // Prevent changes if the same page was selected
    if (pageNumber === this.currentPage) {
      return;
    }

    this.goToPage.emit(pageNumber);
  }
}

// Function to paginate json arrays
export function paginate(
  totalItems: number,
  currentPage: number = 1,
  pageSize: number = 10,
  maxPages: number = 10
) {

  let startPage: number;
  let endPage: number;

  // Calculate total pages
  const totalPages = Math.ceil(totalItems / pageSize);

  // Ensure current page isn't out of range
  if (currentPage < 1) {
    currentPage = 1;
  } else if (currentPage > totalPages) {
    currentPage = totalPages;
  }

  if (totalPages <= maxPages) {

    // Total pages less than max so show all pages
    startPage = 1;
    endPage = totalPages;
  } else {

    // Total pages more than max so calculate start and end pages
    const maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
    const maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;

    if (currentPage <= maxPagesBeforeCurrentPage) {

      // Current page near the start
      startPage = 1;
      endPage = maxPages;
    } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {

      // Current page near the end
      startPage = totalPages - maxPages + 1;
      endPage = totalPages;
    } else {

      // Current page somewhere in the middle
      startPage = currentPage - maxPagesBeforeCurrentPage;
      endPage = currentPage + maxPagesAfterCurrentPage;
    }
  }

  // Calculate start and end item indexes
  const startIndex = (currentPage - 1) * pageSize;
  const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

  // Create an array of pages to ng-repeat in the pager control
  const pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

  // Return object with all pager properties required by the view
  return {
    totalItems,
    currentPage,
    pageSize,
    totalPages,
    startPage,
    endPage,
    startIndex,
    endIndex,
    pages
  };
}
