import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Order } from './model/order';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  // Define function to get either import or existing orders
  loadOrders(
    type: string = 'import',
    pageNumber: number = 1,
    searchValue: string = ''
  ): Observable<any[]> {

    const pageAsString = pageNumber.toString();
    let params: any;
    let orderDataUrl = '';

    // Define get call url based on requested data type
    if (type === 'existing') {
      orderDataUrl = '/api/existing-orders';
    } else if (type === 'import') {
      orderDataUrl = '/api/import-orders';
    }

    // Set different html params if search value is defined or not
    if (searchValue !== '') {

      params = new HttpParams()
        .set('size', '10')
        .set('page', pageAsString)
        .set('search', searchValue);
    } else {

      params = new HttpParams()
        .set('size', '10')
        .set('page', pageAsString);
    }

    // Return http get call to defined url by using defined http params and appy error handling
    return this.http.get<any[]>(orderDataUrl, {params}).pipe(
        retry(3), catchError(this.handleError<any[]>('loadOrders', []))
    );
  }

  // Define function for saving imported order by executing put call
  saveOrder(importOrder: Order) {

    const id: number = importOrder.id;
    const headers = new HttpHeaders()
      .set('X-Auth', 'userId');

    // Return http put call by applying defined order id and object
    return this.http.put(`/api/existing-orders/${id}`, importOrder, {headers}).pipe(
      retry(3), catchError(this.handleError<any[]>('saveOrder', []))
    );
  }

  // Handle http request errors
  private handleError<T>(operation = 'operation', result?: T) {

    return (error: any): Observable<T> => {

      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  // Define error log function
  private log(message: string) {
    console.log(message);
  }
}
