import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Order, Product } from '../model/order';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ModalService } from '../modal';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-import-orders',
  templateUrl: './import-orders.component.html',
  styleUrls: ['./import-orders.component.less']
})
export class ImportOrdersComponent implements OnInit, OnDestroy {

  importOrdersAndPagination$: Observable<Order[]>;
  findOrderForm: FormGroup;
  prepareProductsForm: FormGroup;
  prepareProductVariantsForm: FormGroup;
  currentStepForm: FormGroup;
  formStepIndex = 1;
  selectedOrder: Order;
  selectedProducts: Product[] = [];
  searchValue = '';
  private searchSubject: Subject<string> = new Subject();
  private currentPage = 1;
  private currentSearchValue = '';

  // Define custom event emitter for order import
  @Output()
  importOrder = new EventEmitter<Order>();

  constructor(private apiService: ApiService, private formBuilder: FormBuilder, private modalService: ModalService) {

    // Define order selection form
    this.findOrderForm = this.formBuilder.group({
      orderID: ['', Validators.required]
    });

    // Define product selection form
    this.prepareProductsForm = this.formBuilder.group({
      productSKUs: this.formBuilder.array([])
    });

    // Define product variant selection form
    this.prepareProductVariantsForm = this.formBuilder.group({
      productVariants: this.formBuilder.array([])
    });

    // Execute search subject function
    this.setSearchSubscription();
  }

  // Get import orders and define current import step form, when component is initialized
  ngOnInit() {

    this.importOrdersAndPagination$ = this.apiService.loadOrders('import');
    this.currentStepForm = this.findOrderForm;
  }

  // Unsubscribe from search subject if this component is destroyed
  ngOnDestroy() {
    this.searchSubject.unsubscribe();
  }

  // Subscribe to search subject
  private setSearchSubscription() {

    this.searchSubject.pipe(
      debounceTime(500)
    ).subscribe((searchValue: string) => {
      this.findOrders(searchValue);
    });
  }

  // Trigger search subject, when typing into search input field
  public triggerSearchSubject(searchTextValue: string) {
    this.searchSubject.next(searchTextValue);
  }

  // Get product selection form input FormArray
  get selectedProductArray() {
    return this.prepareProductsForm.get('productSKUs') as FormArray;
  }

  // Get product variant selection form input FormArray
  get selectedProductVariantArray() {
    return this.prepareProductVariantsForm.get('productVariants') as FormArray;
  }

  // Select order on radio input click
  selectOrder(order: Order) {

    // Define selected order, clear selectedProducts array, clear selected product and product variant form input FormArrays
    this.selectedOrder = order;
    this.selectedProducts = [];
    this.selectedProductArray.clear();
    this.selectedProductVariantArray.clear();

    // @ts-ignore
    // Add product inputs to product selection form, based on chosen order. All the inputs are marked as required true
    for (const i of this.selectedOrder.products) {
      this.selectedProductArray.push(new FormControl('', [Validators.requiredTrue]));
    }
  }

  // Select or deselect product on input checkbox click
  selectProduct(e, product: Product) {

    const thisFormControls = this.selectedProductArray.controls;

    // If any of the product selection form checkboxes have been checked
    if (thisFormControls.some((formControl: FormControl) => formControl.value)) {

      // Disable validators for the checkboxes that are not checked
      thisFormControls.forEach((item: FormControl) => {

        if (!item.value || item.value === '') {

          item.setValidators(null);
          item.updateValueAndValidity();
        }
      });

      // If none of the product selection form checkboxes have been checked
    } else {

      // Mark all checkboxes as required true
      thisFormControls.forEach((item: FormControl) => {

        item.setValidators([Validators.requiredTrue]);
        item.updateValueAndValidity();
      });
    }

    // If clicked product checkbox is checked
    if (e.target.checked) {

      // Add this product to selectedProducts array (if it's not already there)
      if (this.selectedProducts.indexOf(product) === -1) {
        this.selectedProducts.push(product);
      }

      // If clicked product checkbox is not checked
    } else {

      // Remove this product from selectedProducts array
      this.selectedProducts = this.selectedProducts.filter(selectedProduct => selectedProduct !== product);
    }

    // Clear product variant form input FormArray
    this.selectedProductVariantArray.clear();

    // Add product variant inputs to product variant selection form, based on selected products
    for (const i of this.selectedProducts) {

      // If the product has variants, mark this FormControl as required
      if ('variants' in i && i.variants.length) {
        this.selectedProductVariantArray.push(new FormControl('', [Validators.required]));

        // If not, add formControl without validation
      } else {
        this.selectedProductVariantArray.push(new FormControl(''));
      }
    }
  }

  // Next / prev form step navigation functionality with option to reset forms
  goToStep(formStepIndex: number, resetForm: boolean = false) {

    this.formStepIndex = formStepIndex;

    if (formStepIndex === 1) {

      this.currentStepForm = this.findOrderForm;

      if (resetForm) {
        this.resetForms();
      }
    } else if (formStepIndex === 2) {
      this.currentStepForm = this.prepareProductsForm;
    } else if (formStepIndex === 3) {
      this.currentStepForm = this.prepareProductVariantsForm;
    }
  }

  // Get import products tat matches search field value
  public findOrders(searchValue: string) {

    this.currentSearchValue = searchValue;

    this.importOrdersAndPagination$ = this.apiService.loadOrders(
      'import',
      this.currentPage,
      this.currentSearchValue
    );
  }

  // Get order list according to clicked page from pagination
  public goToPage(page: number) {

    this.currentPage = page;

    this.importOrdersAndPagination$ = this.apiService.loadOrders(
      'import',
      this.currentPage,
      this.currentSearchValue
    );
  }

  // Close opened modal
  closeModal(id: string) {
    this.modalService.close(id);
    this.goToStep(1);
    this.resetForms();
  }

  // Close opened modal and emit order object that needs to be imported
  closeModalAndImportOrder(id: string) {

    // Define imported order constant
    const importOrder = this.selectedOrder;
    let i = 0;

    // @ts-ignore
    // Add selected products to imported order
    importOrder.products = this.selectedProducts;

    // Add only selected product variants to imported order products
    for (const variant of this.selectedProductVariantArray.controls) {

      // @ts-ignore
      // Clear this product existing variants
      importOrder.products[i].variants = [];

      // @ts-ignore
      // Push selected variants to this product variant array
      importOrder.products[i].variants.push({name: variant.value});
      i++;
    }

    this.importOrder.emit(importOrder);
    this.closeModal(id);
  }

  // Reset forms, selected order, products and search field
  resetForms() {
    this.findOrderForm.reset();
    this.prepareProductsForm.reset();
    this.prepareProductVariantsForm.reset();
    // @ts-ignore
    this.selectedOrder = [];
    this.selectedProducts = [];

    if (this.searchValue !== '') {
      this.searchValue = '';
      this.triggerSearchSubject('');
    }
  }
}
