﻿import { Component, ViewEncapsulation, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';
import { ModalService } from './modal.service';

@Component({
    selector: 'app-modal',
    templateUrl: 'modal.component.html',
    styleUrls: ['modal.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {

  private readonly element: any;

  // Define component input
  @Input() id: string;

  constructor(private modalService: ModalService, private el: ElementRef) {
    this.element = el.nativeElement;
  }

  ngOnInit() {

    // Ensure id attribute exists
    if (!this.id) {

      console.error('modal must have an id');

      return;
    }

    // Move element to bottom of page (just before </body>) so it can be displayed above everything else
    document.body.appendChild(this.element);

    // Close modal on background click
    this.element.addEventListener('click', el => {

      if (el.target.id === 'modalWrapper') {
          this.close();
      }
    });

    // Add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
  }

  // Remove self from modal service when component is destroyed
  ngOnDestroy() {

    this.modalService.remove(this.id);
    this.element.remove();
  }

  // Open modal
  open() {

    this.element.style.display = 'block';
    document.body.classList.add('modalOpen');
  }

  // Close modal
  close() {

    this.element.style.display = 'none';
    document.body.classList.remove('modalOpen');
  }
}
