import * as express from 'express';
import { Application } from 'express';
import { existingOrders } from './server/existing-orders.route';
import { importOrders } from './server/import-orders.route';
import { saveOrder } from './server/save-order.route';

const bodyParser = require('body-parser');
const app: Application = express();

app.use(bodyParser.json());
app.route('/api/existing-orders').get(existingOrders);
app.route('/api/existing-orders/:id').put(saveOrder);
app.route('/api/import-orders').get(importOrders);

const httpServer = app.listen(9000, () => {
    console.log("HTTP REST API Server running at http://localhost:" + httpServer.address().port);
});
